/*
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  Copyright © 2018 Yamashita,Takahiro
 */
package org.vermeerlab.beanvalidation.messageinterpolator;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.validation.ConstraintViolation;
import spec.interfaces.presentation.view.Form;
import spec.annotation.presentation.view.Label;

/**
 * 検証結果からLabel情報を取得するクラスです.
 *
 * @author Yamashita,Takahiro
 */
class LabelFilter {

    private final ConstraintViolation<?> constraintViolation;

    private final List<String> pathes;

    private final List<String> labels;

    private int deepIndex;

    private LabelFilter(ConstraintViolation<?> constraintViolation, List<String> paths) {
        this.constraintViolation = constraintViolation;
        this.pathes = paths;
        this.labels = new ArrayList<>();
        this.deepIndex = 0;
    }

    /**
     * インスタンスを構築します.
     *
     * @param constraintViolation validatorの検証結果
     * @return 構築したインスタンス
     */
    static LabelFilter of(ConstraintViolation<?> constraintViolation) {
        List<String> pathes = Arrays.asList(constraintViolation.getPropertyPath().toString().split("\\."));
        return new LabelFilter(constraintViolation, pathes);
    }

    /**
     * Labelに設定されている値を取得します.
     *
     * @return Labelに設定されている値
     */
    String filter() {
        Class<?> clazz = this.constraintViolation.getRootBeanClass();
        this.recursiveFilter(clazz, this.pathes.get(deepIndex));

        Class<?> invalidClazz = constraintViolation.getInvalidValue().getClass();
        addLabelAnnotatedClass(invalidClazz);
        addClassImplimentsFormObject(invalidClazz);

        return this.labels.isEmpty() ? "" : this.labels.get(0);
    }

    //
    void recursiveFilter(Class<?> clazz, String property) {

        addLabelAnnotatedClass(clazz);
        addClassImplimentsFormObject(clazz);
        addLabelAnnotatedField(clazz, property);

        try {
            Class<?> nextClass = clazz.getDeclaredField(property).getType();
            if (deepIndex < this.pathes.size() - 1) {
                deepIndex++;
                this.recursiveFilter(nextClass, this.pathes.get(deepIndex));
            }
        } catch (NoSuchFieldException | SecurityException ex) {
        }

    }

    //
    void addLabelAnnotatedClass(Class<?> clazz) {
        Label classLabel = clazz.getAnnotation(Label.class);
        if (classLabel != null) {
            this.labels.add(classLabel.value());
        }
    }

    //
    void addClassImplimentsFormObject(Class<?> clazz) {
        if (Form.class.isAssignableFrom(clazz)) {
            this.labels.add(clazz.getCanonicalName() + ".label");
        }
    }

    //
    void addLabelAnnotatedField(Class<?> clazz, String property) {
        try {
            Field field = clazz.getDeclaredField(property);
            Label fieldLabel = field.getAnnotation(Label.class);
            if (fieldLabel != null) {
                this.labels.add(fieldLabel.value());
            }
        } catch (NoSuchFieldException | SecurityException ex) {
        }
    }
}
