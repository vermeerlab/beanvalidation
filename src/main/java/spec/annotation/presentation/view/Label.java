package spec.annotation.presentation.view;

import static java.lang.annotation.ElementType.*;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Formの項目名を指定します.
 * <p>
 * PropertyFileのKeyを指定することで変換することが可能です（国際化対応ができます）.<br>
 * また、LabelをAnnotateしなくとも、PropertyFileにkey値として<code>CLassPath + ".label"</code>を記述することで変換することも可能です.<br>
 * PropertyFileにKeyが存在しない場合は、指定の値をそのまま出力値として使用します.<br>
 *
 * @author Yamashita,Takahiro
 */
@Target({TYPE, FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface Label {

    String value() default "";
}
