/*
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  Copyright © 2018 Yamashita,Takahiro
 */
package org.vermeerlab.beanvalidation.it;

import java.util.Set;
import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import javax.validation.metadata.BeanDescriptor;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import org.junit.Test;
import org.vermeerlab.beanvalidation.it.t000.FormValidation;
import org.vermeerlab.beanvalidation.it.t000.ValidationPriority;
import org.vermeerlab.beanvalidation.validator.GroupSequenceValidator;

/**
 * for coverage
 *
 * @author Yamashita,Takahiro
 */
public class GroupSequenceValidatorTest {

    /**
     * 優先度が一番高い要素でエラーになったら以降の検証が行わない
     */
    @Test
    public void t301a() {
        org.vermeerlab.beanvalidation.it.t301.Page page = new org.vermeerlab.beanvalidation.it.t301.Page();
        org.vermeerlab.beanvalidation.it.t301.ViewForm form = new org.vermeerlab.beanvalidation.it.t301.ViewForm("", "");
        page.setItem(form);

        Validator validator = new GroupSequenceValidator(ValidationPriority.class);
        Set<ConstraintViolation<org.vermeerlab.beanvalidation.it.t301.Page>> results = validator.validate(page);

        assertThat(results.size(), is(1));

        long resultcnt = results.stream()
                .map(ConstraintViolation::getMessage)
                //.peek(System.out::println)
                .filter(result -> {
                    return result.equals("FormValidation.classで制御");
                }).count();

        assertThat(resultcnt, is(1L));
    }

    /**
     * 優先度が一番高い要素でエラーになったら以降の検証が行わない
     */
    @Test
    public void t301b() {
        org.vermeerlab.beanvalidation.it.t301.Page page = new org.vermeerlab.beanvalidation.it.t301.Page();
        org.vermeerlab.beanvalidation.it.t301.ViewForm form = new org.vermeerlab.beanvalidation.it.t301.ViewForm("1", "");
        page.setItem(form);

        Validator validator = new GroupSequenceValidator(ValidationPriority.class);
        Set<ConstraintViolation<org.vermeerlab.beanvalidation.it.t301.Page>> results = validator.validate(page);

        assertThat(results.size(), is(1));

        long resultcnt = results.stream()
                .map(ConstraintViolation::getMessage)
                //.peek(System.out::println)
                .filter(result -> {
                    return result.equals("FormValidation1.classで制御");
                }).count();

        assertThat(resultcnt, is(1L));
    }

    /**
     * 優先度が一番高い要素でエラーになったら以降の検証が行わない
     */
    @Test
    public void t301c() {
        org.vermeerlab.beanvalidation.it.t301.Page page = new org.vermeerlab.beanvalidation.it.t301.Page();
        org.vermeerlab.beanvalidation.it.t301.ViewForm form = new org.vermeerlab.beanvalidation.it.t301.ViewForm("1", "1");
        page.setItem(form);

        Validator validator = new GroupSequenceValidator(ValidationPriority.class);

        Set<ConstraintViolation<org.vermeerlab.beanvalidation.it.t301.Page>> results = validator.validate(page);

        assertThat(results.size(), is(1));

        long resultcnt = results.stream()
                .map(ConstraintViolation::getMessage)
                //.peek(System.out::println)
                .filter(result -> {
                    return result.equals("must be greater than or equal to 100");
                }).count();

        assertThat(resultcnt, is(1L));
    }

    /**
     * 優先度が一番高い要素でエラーになったら以降の検証が行わない（複数項目が対象）
     */
    @Test
    public void t302a() {
        org.vermeerlab.beanvalidation.it.t302.Page page = new org.vermeerlab.beanvalidation.it.t302.Page();
        org.vermeerlab.beanvalidation.it.t302.ViewForm form = new org.vermeerlab.beanvalidation.it.t302.ViewForm("", "", "");
        page.setItem(form);

        Validator validator = new GroupSequenceValidator(ValidationPriority.class);
        Set<ConstraintViolation<org.vermeerlab.beanvalidation.it.t302.Page>> results = validator.validate(page);

        assertThat(results.size(), is(2));

        long resultcnt = results.stream()
                .map(ConstraintViolation::getMessage)
                //.peek(System.out::println)
                .filter(result -> {
                    return result.equals("FormValidation.classで制御1")
                           || result.equals("FormValidation.classで制御2");
                }).count();

        assertThat(resultcnt, is(2L));
    }

    // 以下 カバレッジのためのテスト.（特殊な拡張はしていないので、カバレッジの担保のみ行う）
    /**
     * For coverage
     */
    @Test
    public void testValidate_GenericType() {
        org.vermeerlab.beanvalidation.it.t301.Page page = new org.vermeerlab.beanvalidation.it.t301.Page();
        org.vermeerlab.beanvalidation.it.t301.ViewForm form = new org.vermeerlab.beanvalidation.it.t301.ViewForm("", "");
        page.setItem(form);

        Validator validator = new GroupSequenceValidator(ValidationPriority.class);
        Set<ConstraintViolation<org.vermeerlab.beanvalidation.it.t301.Page>> results = validator.validate(page, FormValidation.class);

        assertThat(results.size(), is(1));

        long resultcnt = results.stream()
                .map(ConstraintViolation::getMessage)
                //.peek(System.out::println)
                .filter(result -> {
                    return result.equals("FormValidation.classで制御");
                }).count();

        assertThat(resultcnt, is(1L));

    }

    /**
     * For coverage
     */
    @Test
    public void testValidateProperty() {
        org.vermeerlab.beanvalidation.it.t303.Page page = new org.vermeerlab.beanvalidation.it.t303.Page();
        org.vermeerlab.beanvalidation.it.t303.ViewForm form = new org.vermeerlab.beanvalidation.it.t303.ViewForm("", "");
        page.setItem(form);

        Validator validator = new GroupSequenceValidator(ValidationPriority.class);
        validator.validateProperty(page, "item");
        validator.validateProperty(page, "item", FormValidation.class);
    }

    /**
     * For coverage
     */
    @Test
    public void testValidateValue() {
        Validator validator = new GroupSequenceValidator(ValidationPriority.class);
        validator.validateValue(org.vermeerlab.beanvalidation.it.t303.ViewForm.class, "property", "");
        validator.validateValue(org.vermeerlab.beanvalidation.it.t303.ViewForm.class, "property", "", FormValidation.class);
    }

    /**
     * For coverage
     */
    @Test
    public void testGetConstraintsForClass() {
        Validator validator = new GroupSequenceValidator(ValidationPriority.class);
        BeanDescriptor beanDescriptor = validator.getConstraintsForClass(org.vermeerlab.beanvalidation.it.t303.ViewForm.class);
    }

    /**
     * For coverage
     */
    @Test
    public void testUnwrap() {
        Validator validator = new GroupSequenceValidator(ValidationPriority.class);
        validator.unwrap(Validator.class);
    }

    /**
     * For coverage
     */
    @Test
    public void testForExecutables() {
        Validator validator = new GroupSequenceValidator(ValidationPriority.class);
        validator.forExecutables();
    }

}
