Custom BeanValidation
===

BeanValidationを使いやすいように独自拡張するライブラリ.

## Description

BeanValidationにより検証をするにあたって機能拡張をするライブラリです。

* 検証とメッセージ変換を分けて行えるようにします
* Form（画面要素）を検証した際に項目名ラベルを付与し、その値をメッセージに適用します
* 複数のリソースを指定できます


## Requirement
事前にローカルリポジトリに `parentpom` プロジェクトを作成してください。

`git clone https://bitbucket.org/vermeerlab/parentpom.git`

## Usage
Maven Repository

https://github.com/vermeerlab/maven/tree/mvn-repo/org/vermeerlab/beanvalidation

## Licence

Licensed under the [Apache License, Version 2.0](http://www.apache.org/licenses/LICENSE-2.0)

## Author

[_vermeer_](https://twitter.com/_vermeer_)