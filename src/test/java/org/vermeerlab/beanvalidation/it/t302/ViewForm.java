/*
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  Copyright © 2018 Yamashita,Takahiro
 */
package org.vermeerlab.beanvalidation.it.t302;

import spec.interfaces.presentation.view.DefaultForm;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import org.vermeerlab.beanvalidation.it.t000.FormValidation;
import org.vermeerlab.beanvalidation.it.t000.FormValidation1;

/**
 *
 * @author Yamashita,Takahiro
 */
public class ViewForm implements DefaultForm<ValueObject> {

    @NotBlank(message = "FormValidation.classで制御1", groups = FormValidation.class)
    private final String value;

    @NotBlank(message = "FormValidation.classで制御2", groups = FormValidation.class)
    private final String value1;

    @NotBlank(message = "FormValidation1.classで制御", groups = FormValidation1.class)
    private final String value2;

    public ViewForm(String value, String value1, String value2) {
        this.value = value;
        this.value1 = value1;
        this.value2 = value2;
    }

    @Valid
    @Override
    public ValueObject getValue() {
        return ValueObject.of(value);
    }

}
