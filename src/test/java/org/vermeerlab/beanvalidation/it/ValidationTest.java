/*
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  Copyright © 2018 Yamashita,Takahiro
 */
package org.vermeerlab.beanvalidation.it;

import java.util.Locale;
import java.util.Set;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import org.junit.Test;
import org.vermeerlab.beanvalidation.it.t000.FormValidation;
import org.vermeerlab.beanvalidation.messageinterpolator.MessageInterpolator;
import org.vermeerlab.beanvalidation.messageinterpolator.MessageInterpolatorFactory;

/**
 *
 * @author Yamashita,Takahiro
 */
public class ValidationTest {

    /**
     * Page ：Label ：なし<br>
     * ViewForm ：不正なし：Label なし<br>
     * ValueObject ：不正なし<br>
     */
    @Test
    public void t001() {
        org.vermeerlab.beanvalidation.it.t001.Page view = new org.vermeerlab.beanvalidation.it.t001.Page();
        org.vermeerlab.beanvalidation.it.t001.ViewForm form = new org.vermeerlab.beanvalidation.it.t001.ViewForm("");
        view.setItem(form);

        Validator validator = Validation.buildDefaultValidatorFactory().getValidator();

        Set<ConstraintViolation<org.vermeerlab.beanvalidation.it.t001.Page>> results
                                                                             = validator.validate(view, FormValidation.class);
        assertThat(results.size(), is(0));
    }

    /**
     * Page ：Label ：なし<br>
     * ViewForm ：不正あり：Label なし：Propertyあり<br>
     * ValueObject ：不正なし<br>
     */
    @Test
    public void t002a() {
        org.vermeerlab.beanvalidation.it.t002a.Page view = new org.vermeerlab.beanvalidation.it.t002a.Page();
        org.vermeerlab.beanvalidation.it.t002a.ViewForm form = new org.vermeerlab.beanvalidation.it.t002a.ViewForm("");
        view.setItem(form);

        Validator validator = Validation.buildDefaultValidatorFactory().getValidator();

        Set<ConstraintViolation<org.vermeerlab.beanvalidation.it.t002a.Page>> results
                                                                              = validator.validate(view, FormValidation.class);

        MessageInterpolatorFactory interpolatorFactory
                                   = MessageInterpolatorFactory.of("Messages", "FormMessages", "FormLabels");

        MessageInterpolator interpolator = interpolatorFactory.create();

        assertThat(results.size(), is(1));

        for (ConstraintViolation<org.vermeerlab.beanvalidation.it.t002a.Page> result : results) {
            String convertedMessage = interpolator.toMessage(result);
            assertThat(convertedMessage, is("空白はダメです"));
        }
    }

    /**
     * Page ：Label ：なし<br>
     * ViewForm ：不正あり：Label なし：Propertyなし（直接指定）<br>
     * ValueObject ：不正なし<br>
     */
    @Test
    public void t002b() {
        org.vermeerlab.beanvalidation.it.t002b.Page view = new org.vermeerlab.beanvalidation.it.t002b.Page();
        org.vermeerlab.beanvalidation.it.t002b.ViewForm form = new org.vermeerlab.beanvalidation.it.t002b.ViewForm("");
        view.setItem(form);

        Validator validator = Validation.buildDefaultValidatorFactory().getValidator();

        Set<ConstraintViolation<org.vermeerlab.beanvalidation.it.t002b.Page>> results
                                                                              = validator.validate(view, FormValidation.class);

        MessageInterpolatorFactory interpolatorFactory
                                   = MessageInterpolatorFactory.of("Messages", "FormMessages", "FormLabels");

        MessageInterpolator interpolator = interpolatorFactory.create();

        assertThat(results.size(), is(1));

        for (ConstraintViolation<org.vermeerlab.beanvalidation.it.t002b.Page> result : results) {
            String convertedMessage = interpolator.toMessage(result);
            assertThat(convertedMessage, is("空白はダメです（直接指定）"));
        }
    }

    /**
     * Page ：Label ：あり：Propertyなし（直接指定）<br>
     * ViewForm ：不正あり：Label なし<br>
     * ValueObject ：不正なし<br>
     */
    @Test
    public void t003a() {
        org.vermeerlab.beanvalidation.it.t003a.Page view = new org.vermeerlab.beanvalidation.it.t003a.Page();
        org.vermeerlab.beanvalidation.it.t003a.ViewForm form = new org.vermeerlab.beanvalidation.it.t003a.ViewForm("");
        view.setItem(form);

        Validator validator = Validation.buildDefaultValidatorFactory().getValidator();

        Set<ConstraintViolation<org.vermeerlab.beanvalidation.it.t003a.Page>> results
                                                                              = validator.validate(view, FormValidation.class);

        MessageInterpolatorFactory interpolatorFactory
                                   = MessageInterpolatorFactory.of("Messages", "FormMessages", "FormLabels");

        MessageInterpolator interpolator = interpolatorFactory.create();

        assertThat(results.size(), is(1));

        for (ConstraintViolation<org.vermeerlab.beanvalidation.it.t003a.Page> result : results) {
            String convertedMessage = interpolator.toMessage(result);
            assertThat(convertedMessage, is("空白はダメです（Viewで直接指定）"));
        }
    }

    /**
     * Page ：Label ：あり：Propertyあり<br>
     * ViewForm ：不正あり：Label なし<br>
     * ValueObject ：不正なし<br>
     */
    @Test
    public void t003b() {
        org.vermeerlab.beanvalidation.it.t003b.Page view = new org.vermeerlab.beanvalidation.it.t003b.Page();
        org.vermeerlab.beanvalidation.it.t003b.ViewForm form = new org.vermeerlab.beanvalidation.it.t003b.ViewForm("");
        view.setItem(form);

        Validator validator = Validation.buildDefaultValidatorFactory().getValidator();

        Set<ConstraintViolation<org.vermeerlab.beanvalidation.it.t003b.Page>> results
                                                                              = validator.validate(view, FormValidation.class);

        MessageInterpolatorFactory interpolatorFactory
                                   = MessageInterpolatorFactory.of("Messages", "FormMessages", "FormLabels");

        MessageInterpolator interpolator = interpolatorFactory.create();

        assertThat(results.size(), is(1));

        for (ConstraintViolation<org.vermeerlab.beanvalidation.it.t003b.Page> result : results) {
            String convertedMessage = interpolator.toMessage(result);
            assertThat(convertedMessage, is("【Property View Label】の空白はダメです"));
        }
    }

    /**
     * Page ：Label ：なし<br>
     * ViewForm ：不正あり：Label あり：Propertyなし（直接指定）<br>
     * ValueObject ：不正なし<br>
     */
    @Test
    public void t004a() {
        org.vermeerlab.beanvalidation.it.t004a.Page view = new org.vermeerlab.beanvalidation.it.t004a.Page();
        org.vermeerlab.beanvalidation.it.t004a.ViewForm form = new org.vermeerlab.beanvalidation.it.t004a.ViewForm("");
        view.setItem(form);

        Validator validator = Validation.buildDefaultValidatorFactory().getValidator();

        Set<ConstraintViolation<org.vermeerlab.beanvalidation.it.t004a.Page>> results
                                                                              = validator.validate(view, FormValidation.class);

        MessageInterpolatorFactory interpolatorFactory
                                   = MessageInterpolatorFactory.of("Messages", "FormMessages", "FormLabels");

        MessageInterpolator interpolator = interpolatorFactory.create();

        assertThat(results.size(), is(1));

        for (ConstraintViolation<org.vermeerlab.beanvalidation.it.t004a.Page> result : results) {
            String convertedMessage = interpolator.toMessage(result);
            assertThat(convertedMessage, is("【Formクラスに直接ラベルを設定】の空白はダメです"));
        }
    }

    /**
     * Page ：Label ：なし<br>
     * ViewForm ：不正あり：Label あり：Propertyあり（FormClassPathあり＋Labelなし）<br>
     * ValueObject ：不正なし<br>
     */
    @Test
    public void t004b() {
        org.vermeerlab.beanvalidation.it.t004b.Page view = new org.vermeerlab.beanvalidation.it.t004b.Page();
        org.vermeerlab.beanvalidation.it.t004b.ViewForm form = new org.vermeerlab.beanvalidation.it.t004b.ViewForm("");
        view.setItem(form);

        Validator validator = Validation.buildDefaultValidatorFactory().getValidator();

        Set<ConstraintViolation<org.vermeerlab.beanvalidation.it.t004b.Page>> results
                                                                              = validator.validate(view, FormValidation.class);

        MessageInterpolatorFactory interpolatorFactory
                                   = MessageInterpolatorFactory.of("Messages", "FormMessages", "FormLabels");

        MessageInterpolator interpolator = interpolatorFactory.create();

        assertThat(results.size(), is(1));

        for (ConstraintViolation<org.vermeerlab.beanvalidation.it.t004b.Page> result : results) {
            String convertedMessage = interpolator.toMessage(result);
            assertThat(convertedMessage, is("【t004bのラベル】の空白はダメです"));
        }
    }

    /**
     * Page ：Label ：なし<br>
     * ViewForm ：不正あり：Label あり：Propertyあり（FormClassPathなし＋Labelあり）<br>
     * ValueObject ：不正なし<br>
     */
    @Test
    public void t004c() {
        org.vermeerlab.beanvalidation.it.t004c.Page view = new org.vermeerlab.beanvalidation.it.t004c.Page();
        org.vermeerlab.beanvalidation.it.t004c.ViewForm form = new org.vermeerlab.beanvalidation.it.t004c.ViewForm("");
        view.setItem(form);

        Validator validator = Validation.buildDefaultValidatorFactory().getValidator();

        Set<ConstraintViolation<org.vermeerlab.beanvalidation.it.t004c.Page>> results
                                                                              = validator.validate(view, FormValidation.class);

        MessageInterpolatorFactory interpolatorFactory
                                   = MessageInterpolatorFactory.of("Messages", "FormMessages", "FormLabels");

        MessageInterpolator interpolator = interpolatorFactory.create();

        assertThat(results.size(), is(1));

        for (ConstraintViolation<org.vermeerlab.beanvalidation.it.t004c.Page> result : results) {
            String convertedMessage = interpolator.toMessage(result);
            assertThat(convertedMessage, is("【t004cラベルあり、PropertyKeyはない】の空白はダメです"));
        }
    }

    /**
     * Page ：Label ：なし<br>
     * ViewForm ：不正あり：Label あり：Propertyあり（FormClassPathあり＋Labelあり）<br>
     * ValueObject ：不正なし<br>
     */
    @Test
    public void t004d() {
        org.vermeerlab.beanvalidation.it.t004d.Page view = new org.vermeerlab.beanvalidation.it.t004d.Page();
        org.vermeerlab.beanvalidation.it.t004d.ViewForm form = new org.vermeerlab.beanvalidation.it.t004d.ViewForm("");
        view.setItem(form);

        Validator validator = Validation.buildDefaultValidatorFactory().getValidator();

        Set<ConstraintViolation<org.vermeerlab.beanvalidation.it.t004d.Page>> results
                                                                              = validator.validate(view, FormValidation.class);

        MessageInterpolatorFactory interpolatorFactory
                                   = MessageInterpolatorFactory.of("Messages", "FormMessages", "FormLabels");

        MessageInterpolator interpolator = interpolatorFactory.create();

        assertThat(results.size(), is(1));

        for (ConstraintViolation<org.vermeerlab.beanvalidation.it.t004d.Page> result : results) {
            String convertedMessage = interpolator.toMessage(result);
            assertThat(convertedMessage, is("【t004dラベルもPropertyKeyある】の空白はダメです"));
        }
    }

    /**
     * Page ：Label ：なし<br>
     * ViewForm ：不正あり：Label あり：Propertyなし（直接指定）<br>
     * ValueObject ：不正なし<br>
     */
    @Test
    public void t005a() {
        org.vermeerlab.beanvalidation.it.t005a.Page view = new org.vermeerlab.beanvalidation.it.t005a.Page();
        org.vermeerlab.beanvalidation.it.t005a.ViewForm form = new org.vermeerlab.beanvalidation.it.t005a.ViewForm("");
        view.setItem(form);

        Validator validator = Validation.buildDefaultValidatorFactory().getValidator();

        Set<ConstraintViolation<org.vermeerlab.beanvalidation.it.t005a.Page>> results
                                                                              = validator.validate(view, FormValidation.class);

        MessageInterpolatorFactory interpolatorFactory
                                   = MessageInterpolatorFactory.of("Messages", "FormMessages", "FormLabels");

        MessageInterpolator interpolator = interpolatorFactory.create();

        assertThat(results.size(), is(1));

        for (ConstraintViolation<org.vermeerlab.beanvalidation.it.t005a.Page> result : results) {
            String convertedMessage = interpolator.toMessage(result);
            assertThat(convertedMessage, is("【t005a_Formクラスで直接指定】の空白はダメです（t005a用にプロパティを準備）"));
        }
    }

    /**
     * Page ：Label ：なし<br>
     * ViewForm ：不正あり：Label あり：Propertyあり（FormClassPathあり＋Labelなし）<br>
     * ValueObject ：不正なし<br>
     */
    @Test
    public void t005b() {
        org.vermeerlab.beanvalidation.it.t005b.Page view = new org.vermeerlab.beanvalidation.it.t005b.Page();
        org.vermeerlab.beanvalidation.it.t005b.ViewForm form = new org.vermeerlab.beanvalidation.it.t005b.ViewForm("");
        view.setItem(form);

        Validator validator = Validation.buildDefaultValidatorFactory().getValidator();

        Set<ConstraintViolation<org.vermeerlab.beanvalidation.it.t005b.Page>> results
                                                                              = validator.validate(view, FormValidation.class);

        MessageInterpolatorFactory interpolatorFactory
                                   = MessageInterpolatorFactory.of("Messages", "FormMessages", "FormLabels");

        MessageInterpolator interpolator = interpolatorFactory.create();

        assertThat(results.size(), is(1));

        for (ConstraintViolation<org.vermeerlab.beanvalidation.it.t005b.Page> result : results) {
            String convertedMessage = interpolator.toMessage(result);
            assertThat(convertedMessage, is("【t005bのプロパティラベル】の空白はダメです（t005b用にプロパティを準備）"));
        }
    }

    /**
     * Page ：Label ：なし<br>
     * ViewForm ：不正あり：Label あり：Propertyあり（FormClassPathなし＋Labelあり）<br>
     * ValueObject ：不正なし<br>
     */
    @Test
    public void t005c() {
        org.vermeerlab.beanvalidation.it.t005c.Page view = new org.vermeerlab.beanvalidation.it.t005c.Page();
        org.vermeerlab.beanvalidation.it.t005c.ViewForm form = new org.vermeerlab.beanvalidation.it.t005c.ViewForm("");
        view.setItem(form);

        Validator validator = Validation.buildDefaultValidatorFactory().getValidator();

        Set<ConstraintViolation<org.vermeerlab.beanvalidation.it.t005c.Page>> results
                                                                              = validator.validate(view, FormValidation.class);

        MessageInterpolatorFactory interpolatorFactory
                                   = MessageInterpolatorFactory.of("Messages", "FormMessages", "FormLabels");

        MessageInterpolator interpolator = interpolatorFactory.create();

        assertThat(results.size(), is(1));

        for (ConstraintViolation<org.vermeerlab.beanvalidation.it.t005c.Page> result : results) {
            String convertedMessage = interpolator.toMessage(result);
            assertThat(convertedMessage, is("【t005c_Formに指定したラベル】の空白はダメです（t005c用にプロパティを準備）"));
        }
    }

    /**
     * Page ：Label ：なし<br>
     * ViewForm ：不正あり：Label あり：Propertyあり（FormClassPathあり＋Labelあり）<br>
     * ValueObject ：不正なし<br>
     */
    @Test
    public void t005d() {
        org.vermeerlab.beanvalidation.it.t005d.Page view = new org.vermeerlab.beanvalidation.it.t005d.Page();
        org.vermeerlab.beanvalidation.it.t005d.ViewForm form = new org.vermeerlab.beanvalidation.it.t005d.ViewForm("");
        view.setItem(form);

        Validator validator = Validation.buildDefaultValidatorFactory().getValidator();

        Set<ConstraintViolation<org.vermeerlab.beanvalidation.it.t005d.Page>> results
                                                                              = validator.validate(view, FormValidation.class);

        MessageInterpolatorFactory interpolatorFactory
                                   = MessageInterpolatorFactory.of("Messages", "FormMessages", "FormLabels");

        MessageInterpolator interpolator = interpolatorFactory.create();

        assertThat(results.size(), is(1));

        for (ConstraintViolation<org.vermeerlab.beanvalidation.it.t005d.Page> result : results) {
            String convertedMessage = interpolator.toMessage(result);
            assertThat(convertedMessage, is("【t005d_Formに指定したラベル】の空白はダメです（t005d用にプロパティを準備）"));
        }
    }

    /**
     * Page ：Label ：なし<br>
     * ViewForm ：不正なし：Label なし<br>
     * ValueObject ：不正あり<br>
     */
    @Test
    public void t006() {
        org.vermeerlab.beanvalidation.it.t006.Page view = new org.vermeerlab.beanvalidation.it.t006.Page();
        org.vermeerlab.beanvalidation.it.t006.ViewForm form = new org.vermeerlab.beanvalidation.it.t006.ViewForm("1");
        view.setItem(form);

        Validator validator = Validation.buildDefaultValidatorFactory().getValidator();

        Set<ConstraintViolation<org.vermeerlab.beanvalidation.it.t006.Page>> formResults
                                                                             = validator.validate(view, FormValidation.class);
        assertThat(formResults.size(), is(0));

        MessageInterpolatorFactory interpolatorFactory
                                   = MessageInterpolatorFactory.of("Messages", "FormMessages", "FormLabels");

        MessageInterpolator interpolator = interpolatorFactory.create();

        Set<ConstraintViolation<org.vermeerlab.beanvalidation.it.t006.Page>> results = validator.validate(view);

        assertThat(results.size(), is(1));

        for (ConstraintViolation<org.vermeerlab.beanvalidation.it.t006.Page> result : results) {
            String convertedMessage = interpolator.toMessage(result);
            assertThat(convertedMessage, is("must be greater than or equal to 100"));
        }
    }

    /**
     * Page ：Label ：あり（直接指定）<br>
     * ViewForm ：不正なし：Label あり（直接指定）<br>
     * ValueObject ：不正あり<br>
     */
    @Test
    public void t007a() {
        org.vermeerlab.beanvalidation.it.t007a.Page view = new org.vermeerlab.beanvalidation.it.t007a.Page();
        org.vermeerlab.beanvalidation.it.t007a.ViewForm form = new org.vermeerlab.beanvalidation.it.t007a.ViewForm("");
        view.setItem(form);

        Validator validator = Validation.buildDefaultValidatorFactory().getValidator();

        Set<ConstraintViolation<org.vermeerlab.beanvalidation.it.t007a.Page>> results
                                                                              = validator.validate(view, FormValidation.class);

        MessageInterpolatorFactory interpolatorFactory
                                   = MessageInterpolatorFactory.of("Messages", "FormMessages", "FormLabels");

        MessageInterpolator interpolator = interpolatorFactory.create();

        assertThat(results.size(), is(1));

        for (ConstraintViolation<org.vermeerlab.beanvalidation.it.t007a.Page> result : results) {
            String convertedMessage = interpolator.toMessage(result);
            assertThat(convertedMessage, is("【t007aView】の空白はダメです（t007a用にプロパティを準備）"));
        }
    }

    /**
     * Page ：Label ：あり（直接指定）<br>
     * ViewForm ：不正なし：Label あり：Propertyあり（FormClassPathなし＋Labelあり）<br>
     * ValueObject ：不正あり<br>
     */
    @Test
    public void t007b() {
        org.vermeerlab.beanvalidation.it.t007b.Page view = new org.vermeerlab.beanvalidation.it.t007b.Page();
        org.vermeerlab.beanvalidation.it.t007b.ViewForm form = new org.vermeerlab.beanvalidation.it.t007b.ViewForm("");
        view.setItem(form);

        Validator validator = Validation.buildDefaultValidatorFactory().getValidator();

        Set<ConstraintViolation<org.vermeerlab.beanvalidation.it.t007b.Page>> results
                                                                              = validator.validate(view, FormValidation.class);

        MessageInterpolatorFactory interpolatorFactory
                                   = MessageInterpolatorFactory.of("Messages", "FormMessages", "FormLabels");

        MessageInterpolator interpolator = interpolatorFactory.create();

        assertThat(results.size(), is(1));

        for (ConstraintViolation<org.vermeerlab.beanvalidation.it.t007b.Page> result : results) {
            String convertedMessage = interpolator.toMessage(result);
            assertThat(convertedMessage, is("【t007bView】の空白はダメです（t007b用にプロパティを準備）"));
        }
    }

    /**
     * Page ：Label ：あり（直接指定）<br>
     * ViewForm ：不正なし：Label あり：Propertyあり（FormClassPathあり＋Labelなし）<br>
     * ValueObject ：不正あり<br>
     */
    @Test
    public void t007c() {
        org.vermeerlab.beanvalidation.it.t007c.Page view = new org.vermeerlab.beanvalidation.it.t007c.Page();
        org.vermeerlab.beanvalidation.it.t007c.ViewForm form = new org.vermeerlab.beanvalidation.it.t007c.ViewForm("");
        view.setItem(form);

        Validator validator = Validation.buildDefaultValidatorFactory().getValidator();

        Set<ConstraintViolation<org.vermeerlab.beanvalidation.it.t007c.Page>> results
                                                                              = validator.validate(view, FormValidation.class);

        MessageInterpolatorFactory interpolatorFactory
                                   = MessageInterpolatorFactory.of("Messages", "FormMessages", "FormLabels");

        MessageInterpolator interpolator = interpolatorFactory.create();

        assertThat(results.size(), is(1));

        for (ConstraintViolation<org.vermeerlab.beanvalidation.it.t007c.Page> result : results) {
            String convertedMessage = interpolator.toMessage(result);
            assertThat(convertedMessage, is("【t007cView】の空白はダメです（t007c用にプロパティを準備）"));
        }
    }

    /**
     * Page ：Label ：あり（直接指定）<br>
     * ViewForm ：不正なし：Label あり：Propertyあり（FormClassPathあり＋Labelあり）<br>
     * ValueObject ：不正あり<br>
     */
    @Test
    public void t007d() {
        org.vermeerlab.beanvalidation.it.t007d.Page view = new org.vermeerlab.beanvalidation.it.t007d.Page();
        org.vermeerlab.beanvalidation.it.t007d.ViewForm form = new org.vermeerlab.beanvalidation.it.t007d.ViewForm("");
        view.setItem(form);

        Validator validator = Validation.buildDefaultValidatorFactory().getValidator();

        Set<ConstraintViolation<org.vermeerlab.beanvalidation.it.t007d.Page>> results
                                                                              = validator.validate(view, FormValidation.class);

        MessageInterpolatorFactory interpolatorFactory
                                   = MessageInterpolatorFactory.of("Messages", "FormMessages", "FormLabels");

        MessageInterpolator interpolator = interpolatorFactory.create();

        assertThat(results.size(), is(1));

        for (ConstraintViolation<org.vermeerlab.beanvalidation.it.t007d.Page> result : results) {
            String convertedMessage = interpolator.toMessage(result);
            assertThat(convertedMessage, is("【t007dView】の空白はダメです（t007d用にプロパティを準備）"));
        }
    }

    /**
     * Page ：Label ：あり：直接指定<br>
     * ViewForm ：不正なし：Label なし<br>
     * ValueObject ：不正あり<br>
     */
    @Test
    public void t008a() {
        org.vermeerlab.beanvalidation.it.t008a.Page view = new org.vermeerlab.beanvalidation.it.t008a.Page();
        org.vermeerlab.beanvalidation.it.t008a.ViewForm form = new org.vermeerlab.beanvalidation.it.t008a.ViewForm("1");
        view.setItem(form);

        Validator validator = Validation.buildDefaultValidatorFactory().getValidator();

        Set<ConstraintViolation<org.vermeerlab.beanvalidation.it.t008a.Page>> results = validator.validate(view);

        MessageInterpolatorFactory interpolatorFactory
                                   = MessageInterpolatorFactory.of("Messages", "FormMessages", "FormLabels");

        MessageInterpolator interpolator = interpolatorFactory.create();

        assertThat(results.size(), is(1));

        for (ConstraintViolation<org.vermeerlab.beanvalidation.it.t008a.Page> result : results) {
            String convertedMessage = interpolator.toMessage(result);
            assertThat(convertedMessage, is("【t008aVIew】は最低100ですよ"));
        }
    }

    /**
     * Page ：Label ：あり：直接指定<br>
     * ViewForm ：不正なし：Label あり<br>
     * ValueObject ：不正あり<br>
     */
    @Test
    public void t009a() {
        org.vermeerlab.beanvalidation.it.t009a.Page view = new org.vermeerlab.beanvalidation.it.t009a.Page();
        org.vermeerlab.beanvalidation.it.t009a.ViewForm form = new org.vermeerlab.beanvalidation.it.t009a.ViewForm("1");
        view.setItem(form);

        Validator validator = Validation.buildDefaultValidatorFactory().getValidator();

        Set<ConstraintViolation<org.vermeerlab.beanvalidation.it.t009a.Page>> results = validator.validate(view);

        MessageInterpolatorFactory interpolatorFactory
                                   = MessageInterpolatorFactory.of("Messages", "FormMessages", "FormLabels");

        MessageInterpolator interpolator = interpolatorFactory.create();

        assertThat(results.size(), is(1));

        for (ConstraintViolation<org.vermeerlab.beanvalidation.it.t009a.Page> result : results) {
            String convertedMessage = interpolator.toMessage(result);
            assertThat(convertedMessage, is("【t009aView】は最低100ですよ"));
        }
    }

    /**
     * Page ：Label ：あり：直接指定<br>
     * ViewForm ：不正あり（メッセージ直接指定）：Label あり<br>
     * ValueObject ：不正なし<br>
     */
    @Test
    public void t010() {
        org.vermeerlab.beanvalidation.it.t010.Page view = new org.vermeerlab.beanvalidation.it.t010.Page();
        org.vermeerlab.beanvalidation.it.t010.ViewForm form = new org.vermeerlab.beanvalidation.it.t010.ViewForm("");
        view.setItem(form);

        Validator validator = Validation.buildDefaultValidatorFactory().getValidator();

        Set<ConstraintViolation<org.vermeerlab.beanvalidation.it.t010.Page>> results = validator.validate(view, FormValidation.class);

        MessageInterpolatorFactory interpolatorFactory
                                   = MessageInterpolatorFactory.of("Messages", "FormMessages", "FormLabels");

        MessageInterpolator interpolator = interpolatorFactory.create();

        assertThat(results.size(), is(1));

        for (ConstraintViolation<org.vermeerlab.beanvalidation.it.t010.Page> result : results) {
            String convertedMessage = interpolator.toMessage(result);
            assertThat(convertedMessage, is("t009aVIewは空白だめ_メッセージ直接指定_t010"));
        }
    }

    /**
     * Page ：Label ：なし<br>
     * ViewForm ：不正あり：Label なし：Propertyあり（FormClassPathあり＋Labelなし）<br>
     * ValueObject ：不正なし<br>
     * Localeを指定
     */
    @Test
    public void t101() {
        org.vermeerlab.beanvalidation.it.t101.Page view = new org.vermeerlab.beanvalidation.it.t101.Page();
        org.vermeerlab.beanvalidation.it.t101.ViewForm form = new org.vermeerlab.beanvalidation.it.t101.ViewForm("");
        view.setItem(form);

        Validator validator = Validation.buildDefaultValidatorFactory().getValidator();

        Set<ConstraintViolation<org.vermeerlab.beanvalidation.it.t101.Page>> results
                                                                             = validator.validate(view, FormValidation.class);

        MessageInterpolatorFactory interpolatorFactory
                                   = MessageInterpolatorFactory.of("Messages", "FormMessages", "FormLabels");

        MessageInterpolator interpolator = interpolatorFactory.create(Locale.JAPAN);

        assertThat(results.size(), is(1));

        for (ConstraintViolation<org.vermeerlab.beanvalidation.it.t101.Page> result : results) {
            String convertedMessage = interpolator.toMessage(result);
            assertThat(convertedMessage, is("【t101_日本語プロパティ】の空白はダメです"));
        }
    }

    /**
     * Page ：Label ：なし<br>
     * ViewForm ：不正なし：Label あり：Propertyあり（FormClassPathなし＋Labelあり）<br>
     * ValueObject ：不正あり<br>
     * Localeを指定
     */
    @Test
    public void t102() {
        org.vermeerlab.beanvalidation.it.t102.Page view = new org.vermeerlab.beanvalidation.it.t102.Page();
        org.vermeerlab.beanvalidation.it.t102.ViewForm form = new org.vermeerlab.beanvalidation.it.t102.ViewForm("");
        view.setItem(form);

        Validator validator = Validation.buildDefaultValidatorFactory().getValidator();

        Set<ConstraintViolation<org.vermeerlab.beanvalidation.it.t102.Page>> results
                                                                             = validator.validate(view, FormValidation.class);

        MessageInterpolatorFactory interpolatorFactory
                                   = MessageInterpolatorFactory.of("Messages", "FormMessages", "FormLabels");

        MessageInterpolator interpolator = interpolatorFactory.create(Locale.JAPAN);

        assertThat(results.size(), is(1));

        for (ConstraintViolation<org.vermeerlab.beanvalidation.it.t102.Page> result : results) {
            String convertedMessage = interpolator.toMessage(result);
            assertThat(convertedMessage, is("【Property View Label（日本語）】の空白はダメです"));
        }
    }

    /**
     * Page ：Label ：なし<br>
     * ViewForm ：不正なし<br>
     * ValueObject ：不正あり（MessagePropertyにないメッセージのもの）<br>
     * Localeを指定
     */
    @Test
    public void t103() {
        org.vermeerlab.beanvalidation.it.t103.Page view = new org.vermeerlab.beanvalidation.it.t103.Page();
        org.vermeerlab.beanvalidation.it.t103.ViewForm form = new org.vermeerlab.beanvalidation.it.t103.ViewForm("1");
        view.setItem(form);

        Validator validator = Validation.buildDefaultValidatorFactory().getValidator();

        Set<ConstraintViolation<org.vermeerlab.beanvalidation.it.t103.Page>> results = validator.validate(view);

        MessageInterpolatorFactory interpolatorFactory
                                   = MessageInterpolatorFactory.of("Messages", "FormMessages", "FormLabels");

        MessageInterpolator interpolator = interpolatorFactory.create(Locale.JAPAN);

        assertThat(results.size(), is(1));

        for (ConstraintViolation<org.vermeerlab.beanvalidation.it.t103.Page> result : results) {
            String convertedMessage = interpolator.toMessage(result);
            assertThat(convertedMessage, is("【Property View Label（日本語）】は最低100ですよ"));
        }
    }

    /**
     * Page ：Label ：なし<br>
     * ViewForm ：不正あり：Label なし：Propertyあり（FormClassPathあり＋Labelなし）<br>
     * ValueObject ：不正なし<br>
     * 複数プロパティを指定
     */
    @Test
    public void t201() {
        org.vermeerlab.beanvalidation.it.t201.Page view = new org.vermeerlab.beanvalidation.it.t201.Page();
        org.vermeerlab.beanvalidation.it.t201.ViewForm form = new org.vermeerlab.beanvalidation.it.t201.ViewForm("");
        view.setItem(form);

        Validator validator = Validation.buildDefaultValidatorFactory().getValidator();

        Set<ConstraintViolation<org.vermeerlab.beanvalidation.it.t201.Page>> results
                                                                             = validator.validate(view, FormValidation.class);

        MessageInterpolatorFactory interpolatorFactory
                                   = MessageInterpolatorFactory.of(
                        new String[]{"Messages", "Messages2"},
                        new String[]{"FormMessages", "FormMessages2"},
                        new String[]{"FormLabels", "FormLabels2"});

        MessageInterpolator interpolator = interpolatorFactory.create();

        assertThat(results.size(), is(1));

        for (ConstraintViolation<org.vermeerlab.beanvalidation.it.t201.Page> result : results) {
            String convertedMessage = interpolator.toMessage(result);
            assertThat(convertedMessage, is("【t201_Message2_Property】の空白はダメです"));
        }
    }

    /**
     * Page ：Label ：なし（メッセージ直接指定）<br>
     * ViewForm ：不正なし：Label あり：Propertyあり（FormClassPathなし＋Labelあり）<br>
     * ValueObject ：不正あり<br>
     * 複数プロパティを指定
     */
    @Test
    public void t202() {
        org.vermeerlab.beanvalidation.it.t202.Page view = new org.vermeerlab.beanvalidation.it.t202.Page();
        org.vermeerlab.beanvalidation.it.t202.ViewForm form = new org.vermeerlab.beanvalidation.it.t202.ViewForm("");
        view.setItem(form);

        Validator validator = Validation.buildDefaultValidatorFactory().getValidator();

        Set<ConstraintViolation<org.vermeerlab.beanvalidation.it.t202.Page>> results
                                                                             = validator.validate(view, FormValidation.class);

        MessageInterpolatorFactory interpolatorFactory
                                   = MessageInterpolatorFactory.of(
                        new String[]{"Messages", "Messages2"},
                        new String[]{"FormMessages", "FormMessages2"},
                        new String[]{"FormLabels", "FormLabels2"});

        MessageInterpolator interpolator = interpolatorFactory.create();

        assertThat(results.size(), is(1));

        for (ConstraintViolation<org.vermeerlab.beanvalidation.it.t202.Page> result : results) {
            String convertedMessage = interpolator.toMessage(result);
            assertThat(convertedMessage, is("【FormLabel2.ViewLabel 】の空白はだめ（メッセージ２）"));
        }
    }

    /**
     * Page ：Label ：なし<br>
     * ViewForm ：不正なし<br>
     * ValueObject ：不正あり（MessagePropertyにないメッセージのもの）<br>
     * 複数プロパティを指定
     */
    @Test
    public void t203() {
        org.vermeerlab.beanvalidation.it.t203.Page view = new org.vermeerlab.beanvalidation.it.t203.Page();
        org.vermeerlab.beanvalidation.it.t203.ViewForm form = new org.vermeerlab.beanvalidation.it.t203.ViewForm("1");
        view.setItem(form);

        Validator validator = Validation.buildDefaultValidatorFactory().getValidator();

        Set<ConstraintViolation<org.vermeerlab.beanvalidation.it.t203.Page>> results = validator.validate(view);

        MessageInterpolatorFactory interpolatorFactory
                                   = MessageInterpolatorFactory.of(
                        new String[]{"Messages", "Messages2"},
                        new String[]{"FormMessages", "FormMessages2"},
                        new String[]{"FormLabels", "FormLabels2"});

        MessageInterpolator interpolator = interpolatorFactory.create();

        assertThat(results.size(), is(1));

        for (ConstraintViolation<org.vermeerlab.beanvalidation.it.t203.Page> result : results) {
            String convertedMessage = interpolator.toMessage(result);
            assertThat(convertedMessage, is("【t203_Message2_Property】は最低100ですよ"));
        }
    }

}
