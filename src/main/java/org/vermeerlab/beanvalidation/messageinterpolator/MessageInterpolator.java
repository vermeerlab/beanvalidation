/*
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  Copyright © 2018 Yamashita,Takahiro
 */
package org.vermeerlab.beanvalidation.messageinterpolator;

import java.util.Collections;
import java.util.Locale;
import java.util.Objects;
import java.util.ResourceBundle;
import javax.validation.ConstraintViolation;
import org.hibernate.validator.internal.engine.MessageInterpolatorContext;
import org.hibernate.validator.messageinterpolation.ResourceBundleMessageInterpolator;
import org.hibernate.validator.spi.resourceloading.ResourceBundleLocator;

/**
 * BeanValidationの検証結果からメッセージを変換するクラスです.
 *
 * @author Yamashita,Takahiro
 */
public class MessageInterpolator {

    private final ResourceBundle messageResourceBundle;
    private final ResourceBundle formMessageResourceBundle;
    private final LabelReader labelReader;

    private MessageInterpolator(ResourceBundle messageResourceBundle, ResourceBundle formMessageResourceBundle, LabelReader labelReader) {
        this.messageResourceBundle = messageResourceBundle;
        this.formMessageResourceBundle = formMessageResourceBundle;
        this.labelReader = labelReader;
    }

    /**
     * インスタンスを構築します.
     *
     * @param messageResourceBundle message（FormLabelなし）のResourceBundle
     * @param formMessageResourceBundle message（FormLabel埋め込みあり）のResourceBundle
     * @param labelResourceBundle FormLabelのResourceBundle
     * @return 構築したインスタンス
     */
    public static MessageInterpolator of(
            ResourceBundle messageResourceBundle, ResourceBundle formMessageResourceBundle, ResourceBundle labelResourceBundle) {

        LabelReader formObjectLabelReader = LabelReader.of(labelResourceBundle);
        return new MessageInterpolator(messageResourceBundle, formMessageResourceBundle, formObjectLabelReader);
    }

    /**
     * 検証結果からメッセージを変換して返却します.
     * <P>
     * 参照するプロパティから一致するキー値が無い場合は、直接指定した値を そのまま返却します.
     *
     * @param constraintViolation validatorの検証結果
     * @return メッセージ
     */
    public String toMessage(ConstraintViolation<?> constraintViolation) {

        ValidatedFormLabel validatedFormLabel = ValidatedFormLabel.of(constraintViolation, this.labelReader);
        ResourceBundleLocator resourceBundleLocator = this.toResourceBundleLocator(validatedFormLabel);
        ResourceBundleMessageInterpolator interpolator = new ResourceBundleMessageInterpolator(resourceBundleLocator);

        MessageInterpolatorContext context = new MessageInterpolatorContext(
                constraintViolation.getConstraintDescriptor(),
                constraintViolation.getInvalidValue(),
                constraintViolation.getRootBeanClass(),
                validatedFormLabel.getMessageParameters(),
                Collections.emptyMap()
        );
        String messageTemplate = constraintViolation.getMessageTemplate();
        return interpolator.interpolate(messageTemplate, context);
    }

    //
    ResourceBundleLocator toResourceBundleLocator(ValidatedFormLabel validatedFormLabel) {
        ResourceBundleLocator resourceBundleLocator = (Locale _locale) -> {
            if (Objects.equals(validatedFormLabel.getLabelValue(), "")) {
                return this.messageResourceBundle;
            } else {
                return this.formMessageResourceBundle;
            }
        };
        return resourceBundleLocator;
    }
}
