/*
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  Copyright © 2018 Yamashita,Takahiro
 */
/**
 * JSR 303 Bean Validation を拡張した機能を提供します.
 * <ul>
 * <li>検証とメッセージ変換を分けて行えるようにする</li>
 * <li>Form（画面要素）を検証した際に項目名ラベルを付与し、その値をメッセージに適用する</li>
 * </ul>
 */
package org.vermeerlab.beanvalidation;
