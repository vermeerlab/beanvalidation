/*
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  Copyright © 2018 Yamashita,Takahiro
 */
package org.vermeerlab.beanvalidation.messageinterpolator;

import java.util.ResourceBundle;

/**
 * FormObjectの項目表示名を取得するクラスです.
 *
 * @author Yamashita,Takahiro
 */
class LabelReader {

    private final ResourceBundle resoureceBundle;

    private LabelReader(ResourceBundle resourceBundle) {
        this.resoureceBundle = resourceBundle;
    }

    static LabelReader of(ResourceBundle labelResourceBundle) {
        return new LabelReader(labelResourceBundle);
    }

    /**
     * FormLabel用のPropertyFileからキーに一致する値を返却します.
     * <P>
     * PropertyFileには、FormObjectのclasspath + ".label"、または Annotationで指定した値 がキーとして実装されていることを想定しています.
     *
     * @param key FormLabel用のPoropertyFileの検索キー
     * @return 検索キーに一致する値. キーが一致する値が無い場合は空白を返却します.
     */
    String getLabelValue(String key) {
        return this.resoureceBundle.containsKey(key)
               ? this.resoureceBundle.getString(key)
               : key.endsWith(".label")
                 ? ""
                 : key;
    }

}
