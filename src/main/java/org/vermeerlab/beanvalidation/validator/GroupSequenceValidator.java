/*
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  Copyright © 2018 Yamashita,Takahiro
 */
package org.vermeerlab.beanvalidation.validator;

import java.util.Set;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.executable.ExecutableValidator;
import javax.validation.metadata.BeanDescriptor;

/**
 * GroupSequence情報を保持したValidatorです.
 * <P>
 * 通常のValidatorは、validateする際に 摘要する validation group を指定しますが、
 * 本クラスはインスタンスを生成する際に 適用する GroupSequenceを保持させておくことで
 * validateの実装簡素化を図れます.
 *
 * @author Yamashita,Takahiro
 * @since 0.4.0
 *
 */
public class GroupSequenceValidator implements Validator {

    final Validator validator;

    final Class<?> groupSequenceType;

    public GroupSequenceValidator(Class<?> groupSequenceType) {
        this.validator = Validation.buildDefaultValidatorFactory().getValidator();
        this.groupSequenceType = groupSequenceType;
    }

    @Override
    public <T> Set<ConstraintViolation<T>> validate(T t, Class<?>... types) {
        if (types.length == 0) {
            return this.validator.validate(t, this.groupSequenceType);
        }
        return this.validator.validate(t, types);
    }

    @Override
    public <T> Set<ConstraintViolation<T>> validateProperty(T t, String string, Class<?>... types) {
        if (types.length == 0) {
            return this.validator.validateProperty(t, string, this.groupSequenceType);
        }

        return this.validator.validateProperty(t, string, types);
    }

    @Override
    public <T> Set<ConstraintViolation<T>> validateValue(Class<T> type, String string, Object o, Class<?>... types) {
        if (types.length == 0) {
            return this.validator.validateValue(type, string, o, this.groupSequenceType);
        }

        return this.validator.validateValue(type, string, o, types);
    }

    @Override
    public BeanDescriptor getConstraintsForClass(Class<?> type) {
        return this.validator.getConstraintsForClass(type);
    }

    @Override
    public <T> T unwrap(Class<T> type) {
        return this.validator.unwrap(type);
    }

    @Override
    public ExecutableValidator forExecutables() {
        return this.validator.forExecutables();
    }

}
