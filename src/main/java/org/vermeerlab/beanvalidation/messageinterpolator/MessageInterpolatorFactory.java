/*
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  Copyright © 2018 Yamashita,Takahiro
 */
package org.vermeerlab.beanvalidation.messageinterpolator;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;
import org.vermeerlab.resourcebundle.CustomControl;

/**
 * BeanValidationの検証結果のメッセージを変換するインスタンスを生成するクラスです.
 * <P>
 * 参照するResourceBundleは複数指定できるので、
 * Propertyを種別毎に作成してメッセージ変換するときに一括で検索できます.
 *
 * @author Yamashita,Takahiro
 */
public class MessageInterpolatorFactory {

    private final List<String> messageResourceBundleNames;
    private final List<String> formMessageResourceBundleNames;
    private final List<String> labelResourceBundleNames;

    private MessageInterpolatorFactory(
            List<String> messageResourceBundleNames, List<String> formMessageResourceBundleNames, List<String> labelResourceBundleNames) {
        this.messageResourceBundleNames = Collections.unmodifiableList(messageResourceBundleNames);
        this.formMessageResourceBundleNames = Collections.unmodifiableList(formMessageResourceBundleNames);
        this.labelResourceBundleNames = Collections.unmodifiableList(labelResourceBundleNames);
    }

    /**
     * インスタンスを構築します.
     *
     * @param messageResourceBundleName メッセージで参照するResouceBundleのBaseName
     * @param formMessageResourceBundleName Formラベル情報を付与したメッセージで参照するResouceBundleのBaseName
     * @param labelResourceBundleName Formのラベル名称を変換する際に参照するResouceBundleのBaseName
     * @return 構築したインスタンス
     */
    public static MessageInterpolatorFactory of(
            String messageResourceBundleName, String formMessageResourceBundleName, String labelResourceBundleName) {
        List<String> messageResourceBundleNames = Arrays.asList(messageResourceBundleName);
        List<String> formMessageResourceBundleNames = Arrays.asList(formMessageResourceBundleName);
        List<String> labelResourceBundleNames = Arrays.asList(labelResourceBundleName);
        return new MessageInterpolatorFactory(messageResourceBundleNames, formMessageResourceBundleNames, labelResourceBundleNames);
    }

    /**
     * Factoryを構築します.
     * <P>
     * 複数の資産を参照対象としたい場合に使用します.
     *
     * @param messageResourceBundleNames メッセージで参照するResouceBundleのBaseName
     * @param formMessageResourceBundleNames Formラベル情報を付与したメッセージで参照するResouceBundleのBaseName
     * @param labelResourceBundleNames Formのラベル名称を変換する際に参照するResouceBundleのBaseName
     * @return 構築したFactory
     */
    public static MessageInterpolatorFactory of(
            String[] messageResourceBundleNames, String[] formMessageResourceBundleNames, String[] labelResourceBundleNames) {
        List<String> _messageResourceBundleNames = Arrays.asList(messageResourceBundleNames);
        List<String> _formMessageResourceBundleNames = Arrays.asList(formMessageResourceBundleNames);
        List<String> _labelResourceBundleNames = Arrays.asList(labelResourceBundleNames);
        return new MessageInterpolatorFactory(_messageResourceBundleNames, _formMessageResourceBundleNames, _labelResourceBundleNames);
    }

    /**
     * メッセージ変換クラスのインスタンスを構築します.
     * <P>
     * ルートロケールを参照対象とします.
     *
     * @return 構築したインスタンス
     */
    public MessageInterpolator create() {
        return this.create(Locale.ROOT);
    }

    /**
     * メッセージ変換クラスのインスタンスを構築します.
     *
     * @param locale 参照したいResourceBundleのLocale
     * @return 構築したインスタンス
     */
    public MessageInterpolator create(Locale locale) {
        ResourceBundle messageResourceBundle = this.getResourceBundle(this.messageResourceBundleNames, locale);
        ResourceBundle formMessageResourceBundles = this.getResourceBundle(this.formMessageResourceBundleNames, locale);
        ResourceBundle labelResourceBundles = this.getResourceBundle(this.labelResourceBundleNames, locale);
        return MessageInterpolator.of(messageResourceBundle, formMessageResourceBundles, labelResourceBundles);
    }

    //
    private ResourceBundle getResourceBundle(List<String> resourceBundleBaseNames, Locale locale) {
        ResourceBundle.Control control = CustomControl.builder().build();
        String baseNames = String.join(",", resourceBundleBaseNames);
        return ResourceBundle.getBundle(baseNames, locale, control);
    }
}
