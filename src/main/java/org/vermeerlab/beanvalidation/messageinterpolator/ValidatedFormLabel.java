/*
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  Copyright © 2018 Yamashita,Takahiro
 */
package org.vermeerlab.beanvalidation.messageinterpolator;

import java.util.HashMap;
import java.util.Map;
import javax.validation.ConstraintViolation;

/**
 * 検証対象の表示に使用されているLabelを参照するクラスです.
 *
 * @author Yamashita,Takahiro
 */
class ValidatedFormLabel {

    private final String labelValue;

    private final LabelReader labelReader;

    private ValidatedFormLabel(String labelValue, LabelReader labelReader) {
        this.labelValue = labelValue;
        this.labelReader = labelReader;
    }

    /**
     * インスタンスを構築します.
     *
     * @param constraintViolation
     * @param labelReader
     * @return 構築したインスタンス
     */
    static ValidatedFormLabel of(ConstraintViolation<?> constraintViolation, LabelReader labelReader) {
        String _labelValue = LabelFilter.of(constraintViolation).filter();
        return new ValidatedFormLabel(_labelValue, labelReader);
    }

    /**
     * FormLabelの値を返却します.
     *
     * @return PropertyFileからキーに一致する値がある場合は、PropertyValueを、無い場合は{@link org.vermeerlab.beanvalidation.form.Label}で設定した値をそのまま返却します.
     */
    String getLabelValue() {
        return this.labelReader.getLabelValue(this.labelValue);
    }

    /**
     * PropertyValueのLabel要素を変換するためのパラメータ形式の情報を返却します.
     * <P>
     * {@link org.hibernate.validator.internal.engine.MessageInterpolatorContext}の引数として指定します.
     *
     * @return 変換用のパラメータ情報
     */
    public Map<String, Object> getMessageParameters() {
        Map<String, Object> map = new HashMap<>();
        map.put("label", this.getLabelValue());
        return map;
    }

}
