/*
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  Copyright © 2018 Yamashita,Takahiro
 */
package org.vermeerlab.beanvalidation.it.t103;

import javax.validation.Valid;
import spec.annotation.presentation.view.Label;
import spec.interfaces.presentation.view.DefaultForm;

/**
 *
 * @author Yamashita,Takahiro
 */
@Label("ViewLabel")
public class ViewForm implements DefaultForm<ValueObject> {

    private final String value;

    public ViewForm(String value) {
        this.value = value;
    }

    public String value() {
        return this.value;
    }

    @Valid
    @Override
    public ValueObject getValue() {
        return ValueObject.of(value);
    }

    @Override
    public String toString() {
        return this.value();
    }

}
